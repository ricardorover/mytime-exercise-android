package com.ricardorover.mytimeexercise.utils;

import android.location.Location;

public class LocationManager {

    private static LocationManager instance;

    public static synchronized LocationManager getInstance() {
        if (instance == null) {
            instance = new LocationManager();
        }
        return instance;
    }

    public Location getCurrentLocation() {
        Location currentLocation = new Location("");
        currentLocation.setLatitude(34.052200);
        currentLocation.setLongitude(-118.242800);
        return currentLocation;
    }

}
