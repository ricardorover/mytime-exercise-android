package com.ricardorover.mytimeexercise.utils;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public class NetworkRequestManager {

    private static NetworkRequestManager instance;
    private Context context;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;

    public static synchronized NetworkRequestManager getInstance(Context context) {
        if (instance == null) {
            instance = new NetworkRequestManager(context);
        }
        return instance;
    }

    private NetworkRequestManager(Context context) {
        this.context = context;
        requestQueue = getRequestQueue();
        imageLoader = new ImageLoader(requestQueue, new LruBitmapCache(context));
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }
}
