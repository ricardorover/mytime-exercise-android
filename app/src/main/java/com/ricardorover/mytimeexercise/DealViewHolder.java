package com.ricardorover.mytimeexercise;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.ricardorover.mytimeexercise.utils.LocationManager;
import com.ricardorover.mytimeexercise.utils.NetworkRequestManager;

import org.json.JSONArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DealViewHolder extends RecyclerView.ViewHolder {

    public String dealId;
    public View rootView;
    public CardView cardView;
    public ImageView thumbImageView;
    public TextView nameView;
    public TextView serviceNameView;
    public TextView distanceView;
    public TextView nextAppointmentView;
    public ImageView instantConfirmationView;
    public View yelpLayout;
    public ImageView yelpLogoImageView;
    public ImageView yelpRatingImageView;
    public ImageView onSaleView;
    public TextView costRangeView;

    public DealViewHolder(View v) {
        super(v);
        rootView = v;
        cardView = (CardView) rootView.findViewById(R.id.deal_cell_card_view);
        thumbImageView = (ImageView) rootView.findViewById(R.id.deal_cell_thumb_image_view);
        nameView = (TextView) rootView.findViewById(R.id.deal_cell_name_view);
        serviceNameView = (TextView) rootView.findViewById(R.id.deal_cell_service_name_view);
        distanceView = (TextView) rootView.findViewById(R.id.deal_cell_distance_view);
        nextAppointmentView = (TextView) rootView.findViewById(R.id.deal_cell_next_appointment_view);
        instantConfirmationView = (ImageView) rootView.findViewById(R.id.deal_cell_instant_confirmation);
        yelpLayout = rootView.findViewById(R.id.deal_cell_yelp_layout);
        yelpRatingImageView = (ImageView) rootView.findViewById(R.id.deal_cell_yelp_rating_image_view);
        yelpLogoImageView = (ImageView) rootView.findViewById(R.id.deal_cell_yelp_logo_image_view);
        onSaleView = (ImageView) rootView.findViewById(R.id.deal_cell_on_sale_view);
        costRangeView = (TextView) rootView.findViewById(R.id.deal_cell_cost_range_view);
    }

    public void prepareForReuse(Context context) {
        int placeholderColor = context.getResources().getColor(R.color.colorPlaceholder);
        thumbImageView.setImageBitmap(null);
        thumbImageView.setBackgroundColor(placeholderColor);
        yelpRatingImageView.setImageBitmap(null);
        yelpRatingImageView.setBackgroundColor(placeholderColor);
        costRangeView.setText(null);
        nextAppointmentView.setText(null);
    }

    public void setupThumbnailAndYelp(final Context context, final Deal deal) {
        setupImageForImageView(context, deal.id, deal.defaultPhotoThumb, thumbImageView);
        if (deal.yelpURL != null && !deal.yelpURL.isEmpty()) {
            setupImageForImageView(context, deal.id, deal.yelpRatingImageURL, yelpRatingImageView);
            yelpLogoImageView.setVisibility(View.VISIBLE);
            yelpRatingImageView.setVisibility(View.VISIBLE);
            yelpLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openURLExternally(context, deal.yelpURL);
                }
            });
        } else {
            yelpLogoImageView.setVisibility(View.GONE);
            yelpRatingImageView.setVisibility(View.GONE);
            yelpLayout.setOnClickListener(null);
        }
    }

    private void setupImageForImageView(final Context context, final String dealId, final String url, final ImageView imageView) {
        NetworkRequestManager.getInstance(context).getImageLoader().get(url, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (DealViewHolder.this.dealId.equals(dealId)) {
                    if (!isImmediate) {
                        Animation fadeIn = new AlphaAnimation(0, 1);
                        fadeIn.setInterpolator(new DecelerateInterpolator());
                        fadeIn.setDuration(200);
                        imageView.startAnimation(fadeIn);
                    }
                    imageView.setImageBitmap(response.getBitmap());
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                if (DealViewHolder.this.dealId.equals(dealId)) {
                    error.printStackTrace();
                }
            }
        });
    }

    public void setNextAppointment(Context context, JSONArray nextAppointments, boolean instantConfirmation) {
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        if (nextAppointments != null && nextAppointments.length() != 0) {
            Date date = null;
            try {
                date = format.parse(nextAppointments.optString(0));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (date != null) {
                nextAppointmentView.setText(String.format(context.getString(R.string.deal_cell_next_appointment), DateFormat.format(context.getString(R.string.deal_cell_next_appointment_date_format), date)));
            }
        }
        instantConfirmationView.setVisibility(instantConfirmation ? View.VISIBLE : View.GONE);
    }

    public void setLocation(Context context, Location location) {
        float distanceInMeters = LocationManager.getInstance().getCurrentLocation().distanceTo(location);
        float distanceInKilometers = distanceInMeters / 1000;
        float distanceInMiles = (float) (distanceInKilometers * 0.621371);
        int distanceFormatRes = R.string.deal_cell_distance;
        if (distanceInMiles > 5) {
            distanceInMiles = (float) Math.round(distanceInMiles);
            distanceFormatRes = R.string.deal_cell_distance_rounded;
        }
        distanceView.setText(String.format(context.getString(distanceFormatRes), distanceInMiles));
    }

    public void setPriceRange(Context context, Integer minPrice, Integer maxPrice, boolean onSale) {
        if (minPrice != null && maxPrice != null) {
            if (minPrice.equals(maxPrice)) {
                costRangeView.setText(String.format(context.getString(R.string.deal_cell_cost_single_format), minPrice));
            } else {
                costRangeView.setText(String.format(context.getString(R.string.deal_cell_cost_range_format), minPrice, maxPrice));
            }
        }
        onSaleView.setVisibility(onSale ? View.VISIBLE : View.GONE);
    }

    public void setURLToOpenOnCardClick(final Context context, final String url) {
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openURLExternally(context, url);
            }
        });
    }

    private void openURLExternally(Context context, String url) {
        if (url != null && !url.isEmpty() && url.startsWith("http")) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(browserIntent);
        }
    }

}