package com.ricardorover.mytimeexercise;

import android.location.Location;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.ricardorover.mytimeexercise.utils.LocationManager;
import com.ricardorover.mytimeexercise.utils.NetworkRequestManager;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static final String RAW_RESPONSE_CACHE_KEY = "RAW_RESPONSE_CACHE";
    private static final String SEARCH_QUERY_KEY = "SEARCH_QUERY";

    private ProgressBar progressBarView;
    private DealsListAdapter adapter;
    private ArrayList<Deal> deals;
    private SearchView searchView;
    private MenuItem searchMenuItem;
    private String searchQuery;
    private String rawResponseCache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        deals = new ArrayList<>();
        searchQuery = "Massage";
        setContentView(R.layout.activity_main);
        progressBarView = (ProgressBar) findViewById(R.id.progress);
        setupToolbar();
        setupRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        dispatchDataRequestIfNeeded();
    }


    //Save and restore state

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(RAW_RESPONSE_CACHE_KEY, rawResponseCache);
        outState.putString(SEARCH_QUERY_KEY, searchQuery);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(RAW_RESPONSE_CACHE_KEY)) {
                rawResponseCache = savedInstanceState.getString(RAW_RESPONSE_CACHE_KEY);
            }
            searchQuery = savedInstanceState.getString(SEARCH_QUERY_KEY);
        }
    }


    //Setup toolbar

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.search_title));
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_bar);
        setupTab(tabLayout, getString(R.string.tab_filter), R.drawable.filter_icon);
        setupTab(tabLayout, getString(R.string.tab_map), R.drawable.map_icon);
    }

    private void setupTab(TabLayout tabLayout, String name, int icon) {
        TabLayout.Tab tab = tabLayout.newTab();
        View tabView = LayoutInflater.from(this).inflate(R.layout.tab, tabLayout, false);
        TextView tabTitleView = (TextView) tabView.findViewById(R.id.text_view);
        tabTitleView.setCompoundDrawablesWithIntrinsicBounds(icon, 0, 0, 0);
        tabTitleView.setText(name);
        tab.setCustomView(tabView);
        tabLayout.addTab(tab);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (!searchView.isIconified()) {
                searchView.setQuery("", false);
                searchView.setIconified(true);
            } else {
                this.finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setQuery("", false);
            searchView.setIconified(true);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchMenuItem.getActionView();// MenuItemCompat.getActionView(searchMenuItem);
        searchView.setOnQueryTextListener(this);
        searchView.setQuery(searchQuery, false);
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                reloadDataWithQuery("");
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextChange(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        reloadDataWithQuery(query);
        return false;
    }

    private void reloadDataWithQuery(String query) {
        if (!query.equals(searchQuery)) {
            deals.clear();
            adapter.notifyDataSetChanged();
            searchQuery = query;
            rawResponseCache = null;
            dispatchDataRequestIfNeeded();
        }
    }


    //Setup RecyclerView and data

    private void setupRecyclerView() {
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new DealsListAdapter(MainActivity.this, deals);
        recyclerView.setAdapter(adapter);
    }

    private void dispatchDataRequestIfNeeded() {
        if (rawResponseCache == null) {
            progressBarView.setVisibility(View.VISIBLE);
            Location currentLocation = LocationManager.getInstance().getCurrentLocation();
            String locationString = currentLocation.getLatitude() + "," + currentLocation.getLongitude();
            String url = "http://www.mytime.com/api/v1/deals.json?what=" + getSearchQueryURLEscaped() + "&when=Anytime&where=" + locationString;
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    rawResponseCache = response.toString();
                    populateDataFromJSONArray(response);
                    progressBarView.setVisibility(View.GONE);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("LOG", "Error: " + error);
                    progressBarView.setVisibility(View.GONE);
                }
            });
            NetworkRequestManager.getInstance(this).addToRequestQueue(jsonArrayRequest);
        } else {
            try {
                populateDataFromJSONArray(new JSONArray(rawResponseCache));
            } catch (JSONException e) {
                e.printStackTrace();
                rawResponseCache = null;
                dispatchDataRequestIfNeeded();
            }
        }
    }

    private String getSearchQueryURLEscaped() {
        try {
            return URLEncoder.encode(searchQuery, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return searchQuery;
        }
    }

    private void populateDataFromJSONArray(JSONArray response) {
        deals.clear();
        for (int index = 0; index < response.length(); index++) {
            deals.add(Deal.fromJSON(response.optJSONObject(index)));
        }
        adapter.notifyDataSetChanged();
    }

}