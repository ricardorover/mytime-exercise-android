package com.ricardorover.mytimeexercise;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class DealsListAdapter extends RecyclerView.Adapter<DealViewHolder> {

    private Context context;
    private ArrayList<Deal> dataSet;

    public DealsListAdapter(Context context, ArrayList<Deal> dataSet) {
        this.context = context;
        this.dataSet = dataSet;
    }

    @Override
    public DealViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.deal_cell, parent, false);
        return new DealViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DealViewHolder holder, int position) {
        holder.prepareForReuse(context);

        Deal deal = dataSet.get(position);
        holder.dealId = deal.id;
        holder.nameView.setText(deal.name);
        holder.serviceNameView.setText(deal.serviceName);
        holder.setLocation(context, deal.location);
        holder.setNextAppointment(context, deal.nextAppointmentTimes, deal.instantConfirmation);
        holder.setupThumbnailAndYelp(context, deal);
        holder.setPriceRange(context, deal.minPrice, deal.maxPrice, deal.onSale);
        holder.setURLToOpenOnCardClick(context, deal.bitlyURL);

        if (position == dataSet.size() - 1) {
            int padding = (int) toPx(10);
            holder.rootView.setPadding(padding, padding, padding, padding);
        }
    }

    private float toPx(float dp) {
        Resources r = context.getResources();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

}