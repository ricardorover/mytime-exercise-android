package com.ricardorover.mytimeexercise;

import android.location.Location;

import org.json.JSONArray;
import org.json.JSONObject;

public class Deal {

    public String id;
    public String name;
    public String serviceName;
    public String bitlyURL;
    public Location location;
    public Integer yelpRating;
    public String yelpRatingImageURL;
    public String yelpURL;
    public Integer minPrice;
    public Integer maxPrice;
    public String defaultPhotoThumb;
    public JSONArray nextAppointmentTimes;
    public boolean instantConfirmation;
    public boolean onSale;

    public static Deal fromJSON(JSONObject dealJSON) {
        Deal deal = new Deal();
        deal.id = dealJSON.optString("id");
        deal.name = dealJSON.optString("name");
        deal.serviceName = dealJSON.optString("service_name");
        deal.bitlyURL = dealJSON.optString("bitly_url");
        deal.setLocationFromJSON(dealJSON.optJSONObject("location"));
        if (dealJSON.has("yelp_rating")) {
            deal.yelpRating = dealJSON.optInt("yelp_rating");
        }
        deal.yelpRatingImageURL = dealJSON.optString("yelp_rating_image_url");
        deal.yelpURL = dealJSON.optString("yelp_url");
        if (dealJSON.has("min_price")) {
            deal.minPrice = dealJSON.optInt("min_price");
        }
        if (dealJSON.has("max_price")) {
            deal.maxPrice = dealJSON.optInt("max_price");
        }
        deal.defaultPhotoThumb = dealJSON.optString("default_photo_thumb");
        deal.nextAppointmentTimes = dealJSON.optJSONArray("next_appointment_times");
        deal.instantConfirmation = dealJSON.optBoolean("instant_confirmation");
        deal.onSale = dealJSON.optBoolean("on_sale");
        return deal;
    }

    private void setLocationFromJSON(JSONObject locationJSON) {
        this.location = new Location("");
        location.setLatitude(locationJSON.optDouble("lat"));
        location.setLongitude(locationJSON.optDouble("lon"));
    }
}
